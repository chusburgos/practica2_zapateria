package com.chus.zapateria.base;

public class Zapas extends Zapateria {

    private String tipoDeporte;

    public Zapas() { super(); }

    public Zapas(String marca, String modelo, int talla, String sexo, String tipoDeporte) {
        super(marca, modelo, talla, sexo);
        this.tipoDeporte = tipoDeporte;
    }

    @Override
    public String toString() {
        return "Zapas-> " + "Marca: " + getMarca() + ", Modelo: " + getModelo()
                + ", Talla: " + getTalla() + ", Sexo: " + getSexo()
                +", Tipo de Deporte: " + tipoDeporte + ".";
    }

    public String getTipoDeporte() {
        return tipoDeporte;
    }

    public void setTipoDeporte(String tipoDeporte) {
        this.tipoDeporte = tipoDeporte;
    }
}
