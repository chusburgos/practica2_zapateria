package gui;

import com.chus.zapateria.base.Zapateria;
import javafx.scene.input.InputMethodTextRun;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton hombreRadioButton;
    public JRadioButton mujerRadioButton;
    public JLabel Modelo;
    public JTextField txtModelo;
    public JLabel Marca;
    public JTextField txtMarca;
    public JLabel Talla;
    public JTextField txtTalla;
    public JLabel sexo;
    public JButton altaZapatoButton;
    public JButton mostrarZapatoButton;
    public JButton importarBtn;
    public JList list1;

    //Elementos creados por mi
    public DefaultListModel<Zapateria> dlmZapato;

    public Ventana() {
        frame = new JFrame("Zapatos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmZapato = new DefaultListModel<Zapateria>();
        list1.setModel(dlmZapato);
    }

}
