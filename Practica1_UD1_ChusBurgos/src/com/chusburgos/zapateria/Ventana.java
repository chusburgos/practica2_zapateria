package com.chusburgos.zapateria;

import com.chusburgos.zapateria.base.Zapatos;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Ventana implements ActionListener {
    private JFrame frame;
    private JPanel panel1;

    private JTextField txtMarca;
    private JTextField txtModelo;
    private JTextField txtTalla;
    private JTextField txtSexo;

    private JButton altaZapatoButton;
    private JButton mostrarListaZapatosButton;

    private JComboBox comboBox;

    private JMenuItem itemExportarXML;
    private JMenuItem itemImportarXML;

    private LinkedList<Zapatos> lista;
    private DefaultComboBoxModel<Zapatos> dcbm;

    public Ventana() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);

        crearMenu();
        anadirDatosComboBox();

        altaZapatoButton.addActionListener(this);
        mostrarListaZapatosButton.addActionListener(this);
        itemImportarXML.addActionListener(this);
        itemExportarXML.addActionListener(this);

    }

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
    }

    private void altaZapato() {
        String marca = txtMarca.getText();
        String modelo = txtModelo.getText();
        int talla = Integer.parseInt(txtTalla.getText());
        String sexo = txtSexo.getText();

        lista.add(new Zapatos(marca, modelo, talla, sexo));
        refrescarComboBox();
    }

    private void mostrarDatosLinkedList() {
        for(Zapatos zapato : lista) {
            System.out.println(zapato);
        }
    }
    private void anadirDatosComboBox() {
        comboBox.addItem("DATOS");
        comboBox.addItem("Deportivas");
        comboBox.addItem("Mocasines");
        comboBox.addItem("Botines y Botas");
        comboBox.addItem("Tacones");
    }

    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Zapatos zapato : lista) {
            dcbm.addElement(zapato);
        }
    }
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemExportarXML = new JMenuItem("Exportar XML");
        itemImportarXML = new JMenuItem("Importar XML");

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);
        barra.add(menu);

        frame.setJMenuBar(barra);
    }

    public void actionPerformed(ActionEvent e) {
        Object botonPulsado = e.getSource();
        if (botonPulsado == altaZapatoButton) {
            altaZapato();
        } else if (botonPulsado == mostrarListaZapatosButton) {
            mostrarDatosLinkedList();
        } else if (botonPulsado == itemExportarXML) {
            JFileChooser selectorArchivo = new JFileChooser();
            int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
            if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                File fichero = selectorArchivo.getSelectedFile();
                exportarXML(fichero);
            }
        } else if (botonPulsado == itemImportarXML) {
            JFileChooser selectorArchivo = new JFileChooser();
            int opcion = selectorArchivo.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION) {
                File fichero = selectorArchivo.getSelectedFile();
                importarXML(fichero);
                refrescarComboBox();
            }

        }
    }
        private void importarXML (File fichero){
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            try {
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document documento = builder.parse(fichero);

                NodeList zapatos = documento.getElementsByTagName("zapatos");
                for(int i = 0; i <zapatos.getLength(); i++) {
                    Node zapato = (Node) zapatos.item(i);
                    Element elemento = (Element) zapato;

                    String marca = elemento.getElementsByTagName("marca").item(0).getChildNodes().item(0).getNodeValue();
                    String modelo = elemento.getElementsByTagName("modelo").item(0).getChildNodes().item(0).getNodeValue();
                    int talla = Integer.parseInt(elemento.getElementsByTagName("talla").item(0).getChildNodes().item(0).getNodeValue());
                    String sexo = elemento.getElementsByTagName("sexo").item(0).getChildNodes().item(0).getNodeValue();
                    altaZapato();
                }
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }


        }

        private void exportarXML (File fichero) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = null;

            try {
                builder = factory.newDocumentBuilder();
                DOMImplementation dom = builder.getDOMImplementation();

                //Creo documento que representa arbol XML
                Document documento = (Document) dom.createDocument(null, "xml", null);

                //Creo el nodo raiz (coches) y lo añado al documento
                Element raiz = documento.createElement("Deportivas");
                documento.getDocumentElement().appendChild(raiz);

                Element nodoZapatos;
                Element nodoDatos;
                Text dato;

                //Por cada coche de la lista, creo un nodo coche
                for (Zapatos zapato : lista) {

                    //Creo un nodo coche y lo añado al nodo raiz (coches)
                    nodoZapatos = documento.createElement("Zapatos");
                    raiz.appendChild(nodoZapatos);

                    //A cada nodo coche le añado los nodos marca y modelo
                    nodoDatos = documento.createElement("marca");
                    nodoZapatos.appendChild(nodoDatos);

                    //A cada nodo de datos le añado el dato
                    dato = documento.createTextNode(zapato.getMarca());
                    nodoDatos.appendChild(dato);

                    nodoDatos = documento.createElement("modelo");
                    nodoZapatos.appendChild(nodoDatos);

                    dato = documento.createTextNode(zapato.getModelo());
                    nodoDatos.appendChild(dato);

                    nodoDatos = documento.createElement("sexo");
                    nodoZapatos.appendChild(nodoDatos);

                    dato = documento.createTextNode(zapato.getSexo());
                    nodoDatos.appendChild(dato);
                }

                //Transformo el documento anterior en un ficho de texto plano
                Source src = new DOMSource(documento);
                Result result = new StreamResult(fichero);

                Transformer transformer = null;
                transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(src, result);

            } catch (TransformerConfigurationException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        }
    }