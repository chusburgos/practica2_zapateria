package com.chusburgos.zapateria.base;

public class Zapatos {

    private String marca;
    private String modelo;
    private int talla;
    private String sexo;

    public Zapatos(String marca, String modelo, int talla, String sexo) {
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
        this.sexo = sexo;
    }
    public Zapatos() {}

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}