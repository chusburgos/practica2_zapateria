package com.chus.zapateria.util;

import gui.Ventana;
import gui.ZapasMarca;
import gui.ZapatosControlador;

public class Principal {

    public static void main(String[] args) {
        System.out.println("1. Creación de la Zapatería");

        Ventana vista = new Ventana();
        ZapasMarca marca = new ZapasMarca();
        ZapatosControlador controlador = new ZapatosControlador(vista, marca);
    }
}
