package gui;

import com.chus.zapateria.base.Zapateria;

import javax.swing.*;

public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton hombreRadioButton;
    public JRadioButton mujerRadioButton;
    public JLabel Modelo;
    public JTextField txtModelo;
    public JLabel Marca;
    public JTextField txtMarca;
    public JLabel Talla;
    public JTextField txtTalla;
    public JLabel sexo;
    public JButton altaZapatoButton;
    public JButton mostrarZapatoButton;
    public JButton importarBtn;
    public JList listZapato;

    //Elementos creados por mi
    public DefaultListModel<Zapateria> dlmZapato;

    public Ventana() {
        frame = new JFrame("Zapatos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmZapato = new DefaultListModel<Zapateria>();
        listZapato.setModel(dlmZapato);
    }

}
