package gui;

import com.chus.zapateria.base.Zapateria;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

public class ZapatosControlador implements ActionListener, ListSelectionListener, WindowListener {
    private Ventana vista;
    private ZapasMarca marca;
    private File ultimaRutaExportada;

    public ZapatosControlador(Ventana vista, ZapasMarca marca) {
        this.vista = vista;
        this.marca = marca;

        try {
            cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }

        addActionListener(this);
        addListSelectionListener(this);
        addWindowListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand) {
            case "Nuevo":
                break;

            case "Importar":
                break;

            case "Exportar":
                break;

        }

    }

    private boolean hayCamposVacios() {
        if (vista.txtModelo.getText().isEmpty() ||
                vista.txtMarca.getText().isEmpty() ||
                vista.txtTalla.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    private void limpiarCampos() {
        vista.txtModelo.setText(null);
        vista.txtMarca.setText(null);
        vista.txtTalla.setText(null);
    }

    private void refrescar() {
        vista.dlmZapato.clear();
        for (Zapateria unZapato: marca.obtenerZapatos()) {
            vista.dlmZapato.addElement(unZapato);
        }
    }

    private void addActionListener(ActionListener listener) {
        vista.hombreRadioButton.addActionListener(listener);
        vista.mujerRadioButton.addActionListener(listener);
        vista.altaZapatoButton.addActionListener(listener);
        //vista.importarBtn.addActionListener(listener);
        vista.mostrarZapatoButton.addActionListener(listener);
    }

    private void addWindowListener(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener) {
        vista.listZapato.addListSelectionListener(listener);
    }

    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("zapatos.conf"));
        ultimaRutaExportada= new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada=ultimaRutaExportada;
    }

    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada",ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("zapatos.conf"),"Datos configuracion zapatos");

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

}
