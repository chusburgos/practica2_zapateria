package com.chus.zapateria.base;

public class Zapateria {

    private String marca;
    private String modelo;
    private String talla;
    private String sexo;

    public Zapateria(String marca, String modelo, String talla, String sexo) {
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
        this.sexo = sexo;
    }
    public Zapateria() {}

    public Zapateria(String marca, String modelo, String talla) {
        this.marca = marca;
        this.modelo = modelo;
        this.talla = talla;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
