package gui;

import com.chus.zapateria.base.Zapas;
import com.chus.zapateria.base.Zapateria;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.print.Doc;
import javax.swing.*;
import javax.swing.text.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Ventana {
    public JFrame frame;
    private JPanel panel1;
    public JLabel Modelo;
    public JTextField txtModelo;
    public JLabel Marca;
    public JTextField txtMarca;
    public JLabel Talla;
    public JTextField txtTalla;
    public JLabel sexo;
    public JRadioButton hombreRadioButton;
    public JRadioButton mujerRadioButton;
    private JComboBox comboBox;
    public JButton altaZapatoButton;
    public JButton mostrarZapatoButton;
    public JList listZapato;
    public JLabel tipoDeporte;
    private JTextField txtTipoDeporte;

    //Elementos creados por mi
    public DefaultListModel<Zapateria> dlmZapato;
    private LinkedList<Zapateria>lista;
    private DefaultComboBoxModel<Zapateria>dcbm;

    public Ventana() {
        frame = new JFrame("Zapatos");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();

        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        comboBox.setModel(dcbm);

        altaZapatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                altaZapato(txtMarca.getText(), txtModelo.getText(), txtTalla.getText(), txtTipoDeporte.getText());
                refrescarComboBox();
            }
        });
    }

    private void altaZapato(String marca, String modelo, String talla, String tipoDeporte) {
        lista.add(new Zapateria(marca, modelo, talla, tipoDeporte));
    }
    private void refrescarComboBox() {
        dcbm.removeAllElements();
        for (Zapateria zapato : lista) {
            dcbm.addElement(zapato);
        }
    }

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
    }

    private void initComponents() {
        dlmZapato = new DefaultListModel<Zapateria>();
        listZapato.setModel(dlmZapato);
    }

    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if(opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if(opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });
        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        NodeList zapas;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = (Document) builder.parse(fichero);

            //Recorro cada uno de los nodos coche para obtener sus campos
            zapas = documento.getElementsByTagName("zapas");
            for (int i = 0; i < zapas.getLength(); i++) {
                Node zapatillas = zapas.item(i);
                Element elemento = (Element) zapatillas;

                //Obtengo los campos marca y modelo
                String marca = elemento.getElementsByTagName("marca").item(0).getChildNodes().item(0).getNodeValue();
                String modelo = elemento.getElementsByTagName("modelo").item(0).getChildNodes().item(0).getNodeValue();
                String talla = elemento.getElementsByTagName("talla").item(0).getChildNodes().item(0).getNodeValue();
                String tipoDeporte = elemento.getElementsByTagName("TipoDeporte").item(0).getChildNodes().item(0).getNodeValue();

                altaZapato(marca, modelo, talla, tipoDeporte);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = (Document) dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (coches) y lo añado al documento
            Element raiz = documento.createElement("Zapas");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoZapas;
            Element nodoDatos;
            Text dato;

            //Por cada coche de la lista, creo un nodo coche
            for (Zapateria zapas : lista) {

                //Creo un nodo coche y lo añado al nodo raiz (coches)
                nodoZapas = documento.createElement("Zapas");
                raiz.appendChild(nodoZapas);

                //A cada nodo coche le añado los nodos marca y modelo
                nodoDatos = documento.createElement("marca");
                nodoZapas.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(zapas.getMarca());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("modelo");
                nodoZapas.appendChild(nodoDatos);

                dato = documento.createTextNode(zapas.getModelo());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("talla");
                nodoZapas.appendChild(nodoDatos);

                dato = documento.createTextNode(zapas.getTalla());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("sexo");
                nodoZapas.appendChild(nodoDatos);

                dato = documento.createTextNode(zapas.getSexo());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource((Node) documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

}
