package gui;

import com.chus.zapateria.base.Zapas;
import com.chus.zapateria.base.Zapateria;
import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class ZapasMarca {
    private ArrayList<Zapateria> listaZapatos;

    public ZapasMarca() {
        listaZapatos = new ArrayList<Zapateria>();
    }

    public ArrayList<Zapateria> obtenerZapatos() {
        return listaZapatos;
    }

    public void altaZapas() {
    }


    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Zapateria");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoZapas = null, nodoDatos = null;
        Text texto = null;

        for (Zapateria unZapato : listaZapatos) {

            nodoZapas = documento.createElement("Zapas");
            raiz.appendChild(nodoZapas);

            /*Dentro de la etiqueta vehiculo le añado
            las subetiquetas con los datos de sus
            atributos (matricula, marca, etc)
             */
            nodoDatos = documento.createElement("marca");
            nodoZapas.appendChild(nodoDatos);

            texto = documento.createTextNode(unZapato.getMarca());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("modelo");
            nodoZapas.appendChild(nodoDatos);

            texto = documento.createTextNode(unZapato.getModelo());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("talla");
            nodoZapas.appendChild(nodoDatos);

            texto = documento.createTextNode(unZapato.getTalla().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de vehiculo
            debo acceder a él controlando el tipo de objeto
             */

            nodoDatos = documento.createElement("tipo-deporte");
            nodoZapas.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Zapas) unZapato).getTipoDeporte()));
            nodoDatos.appendChild(texto);


        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException, org.xml.sax.SAXException {
        listaZapatos = new ArrayList<Zapateria>();
        Zapas nuevasZapas = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoZapatilla = (Element) listaElementos.item(i);

            nuevasZapas = new Zapas();
            nuevasZapas.setMarca(nodoZapatilla.getChildNodes().item(0).getTextContent());
            nuevasZapas.setModelo(nodoZapatilla.getChildNodes().item(1).getTextContent());
            nuevasZapas.setTalla(nodoZapatilla.getChildNodes().item(2).getTextContent());
            nuevasZapas.setSexo(nodoZapatilla.getChildNodes().item(3).getTextContent());
            nuevasZapas.setTipoDeporte(nodoZapatilla.getChildNodes().item(4).getTextContent());
            listaZapatos.add(nuevasZapas);


        }
    }
}

